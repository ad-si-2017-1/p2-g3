var Sala = ""; // SALA ATUAL


// INFORMA O USUARIO O TIPO DE INFORMAÇÃO ESPERADA
function setBtnComando() {
    $('#Valor').val("");
    // VERIFICA SE FOI FORNECIDO UM SERVIDOR
    if ($('#Server').val() == "") {
        $('#Comando').prop('value', "Set Servidor");
    }
    else
    // VERIFICA SE FOI FORNECIDO UM NICK
    if ($('#Nick').val() == "") {
        $('#Comando').prop('value', "Set NICK");
    }
    else
    // VERIFICA SE FOI FORNECIDO UMA SALA
    if ($('#Channel').val() == "") {
        $('#Comando').prop('value', "New Channel");
    }
}

// mautec 
// ATUALIZA SALAS DISPONIVEIS
function atualizaSalas (relacao) {
    for (var item in relacao) { 
        var texto = "<li id='" + relacao[item] + "' onclick='trocaSala(\"" + relacao[item] + "\")'>" + relacao[item] + "</li>";
        $('#dvCanal').append(texto);
        }
    $('#dvUsuario').html("");
}

// mautec 
// ATUALIZA USUARIOS DE UMA SALA
function atualizaUsuarios (relacao) {
    for (var item in relacao) { 
        var texto = "<li>" + relacao[item] + "</li>";
        $('#dvUsuario').append(texto);
        }
}

// matheus
function channelList_Start() {
    $.get("obter_lista", function (data, status) {
        if (!data) {
            channelList_Start();
        } else {
            var lista = data;
            console.log(lista[1].name);
            for (var i = 0; i < lista.length; i++) {
                var node = document.createElement("LI");
                var textnode = document.createTextNode(JSON.stringify(lista[i]));
                node.appendChild(textnode);
                document.getElementById("lista1").appendChild(node);
            }
        }

    });
}

//matheus
function channelList() {
    $.get("obter_list_usuario", function (data, status) {
        var lista;
        if (data != "undefined") {
            document.getElementById("estado").innerHTML = "Usuarios conectados";
            for (var i = 0; i < data.length; i++) {
                //	lista += data[i]+"";
                var node = document.createElement("LI");
                var textnode = document.createTextNode(data[i]);
                node.appendChild(textnode);
                document.getElementById("lista").appendChild(node);
            }

        } else {
            document.getElementById("estado").innerHTML = "Carregando lista de usuarios";
        }
    });
    $('ul').empty();
    t = setTimeout(function () {
        channelList();
    }, 1000);
}


// mautec
// ATIVA O RECEBIMENTO E ENVIO
// DE MENSAGENS PARA A SALA ESCOLHIDA
function trocaSala (sala) {
    Sala = sala;
    var info = '<h4>' +  $('#Server').val() + '</h4>';
    $('#SalaMural').html(info + "<h3>"+Sala+"</h3>");
        
    // Aqui outros processamento com relaçao ao IRC"
    alert("INFORMAR TROCA DE SALA NODEJS");
}

// mautec
// CAPTURA OS VALORES DE edtValor E PREENCHE OS CAMPOS HIDDEN 
// DO FORMULARIO
function setFormValues() {
    if ($('#Valor').val() == "") return;

    //  SE FOI FORNECIDO UM SERVIDOR
    if ($('#Server').val() == "") {
        $('#Server').val($('#Valor').val());
        var info = '<h4>' +  $('#Valor').val() + '</h4>';
        $('#SalaMural').html(info);
        setBtnComando();
        var servidor = $('#Valor').val();
    }
    else
    // VERIFICA SE FOI FORNECIDO UM NICK
    if ($('#Nick').val() == "") {
        $('#Nick').val($('#Valor').val());
        var info = '<h4>' + $('#Valor').val() + '</h4>';
        $('#UsrInfo').html(info);
        setBtnComando();
        var nick = $('#Valor').val();
        conectaIRC();
    }
    else
    // VERIFICA SE FOI FORNECIDO UMA SALA
    if ($('#Channel').val() == "") {
        $('#Channel').val($('#Valor').val());
        setBtnComando();
        var canal = $('#Valor').val();
    }
}

// mautec
function getUsers() {
    alert ('PROCESSAMENTO PARA BUSCAR USUARIOS DA SALA');

}

// mautec
function getSalas(){ 
    alert ('PROCESSAMENTO PARA BUSCAR SALAS DISPONIVEIS');
}

// mautec
function setStealth() {
    alert ('PROCESSAMENTO PARA ENTRAR EM MODO INVISIVEL');
} 



//  Adiciona <mensagem> no <elemento_id>. 
function adiciona_mensagem(mensagem,elemento_id,timestamp) {
	var novo_elemento = document.createElement('div');
	novo_elemento.id = "mensagem"+timestamp;
	document.getElementById(elemento_id).appendChild(novo_elemento);
	document.getElementById('mensagem'+timestamp).innerHTML=mensagem;
}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}

function iniciar(elemento_id) {
	$("#status").text("Conectado - irc://"+
			Cookies.get("nick")+"@"+
			Cookies.get("servidor")+"/"+
			Cookies.get("canal"));
	carrega_mensagens(elemento_id,0);
	channelList();

}

/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp="0";
function carrega_mensagens(elemento_id, timestamp) {
	var mensagem = "";
	var horario = "";
	$.get("obter_mensagem/"+timestamp, function(data,status) {
		if ( status == "success" ) {
		    var linhas = data;
		    for ( var i = linhas.length-1; i >= 0; i-- ) {
		    	horario = timestamp_to_date(linhas[i].timestamp);
			mensagem = 
				"["+horario+" - "+
				linhas[i].nick+"]: "+
		                linhas[i].msg;
			novo_timestamp = linhas[i].timestamp;
		    	adiciona_mensagem(mensagem,elemento_id,novo_timestamp);
			}
		}
		else {
		    alert("erro: "+status);
		}
		}
	);
	t = setTimeout( 
		function() { 
			carrega_mensagens(elemento_id,novo_timestamp) 
		}, 
		1000);		
}

// matheus
function funcoes(mensagem) {
    var opcao = mensagem.split(" ");
    switch (opcao[0]) {
        case "/list":
            $.get("opcao/lista_canais", function (data, status) {   
                var terminou = data;
                if (!terminou)
                    channelList_Start();
            });
            break;
        case "/nick":
            var arg = opcao[1];
            $.get("opcao/mudar_nick/" + arg + "", function (data, status) {
                Cookies.set('nick', data);
                iniciar("mensagem");
            });
            break;
        case "/join":
            var arg = opcao[1];
            $.get("opcao/entrar_canal/" + arg + "", function (data, status) {
                Cookies.set('canal', data);
                iniciar("mensagem");
            });
            break;
        default:
    }
}

// mautec / matheus
function conectaIRC() {
$('#vChannel').value = "irc.freenode.net";
alert("connectaIRC");

    var msg = '{"vServer":"' + $('#vServer').val() + '","vChannel":"' + $('#vChannel').val() + '","vNick":"' + $('#vNick').val() + '"}';               

    // CONFIGURA OS COOKIES
	$.ajax({
		type: "post",
		url: "/login", 
		data: msg,
		success: 
		function(data,status) {
			if (status == "success") {
			    alert ("COOKIES CONFIGURADOS");
			    // nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});

    // CONECTA AO IRC
	$.ajax({
		type: "get",
		url: "/", 
		success: 
		function(data,status) {
			if (status == "success") {
			    alert ("CONECTADO IRC");
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});

}

/*
   Submete a mensagem dos valores contidos s elementos identificados 
   como <elem_id_nome> e <elem_id_mensagem>
*/
function submete_mensagem(elem_id_mensagem) {
	var mensagem= document.getElementById(elem_id_mensagem).value;
	var msg = '{"timestamp":'+Date.now()+','+
		  '"nick":"'+Cookies.get("nick")+'",'+
                  '"msg":"'+mensagem+'"}';
	$.ajax({
		type: "post",
		url: "/gravar_mensagem",
		data: msg,
		success: 
		function(data,status) {
			if (status == "success") {
			    // nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});
}

function trocarMode(elemento){

	var usuario = Cookies.get("nick");
	var args = $("#"+elemento).val();
	var comando = "mode/"+usuario+"/"+args;
    $.get(comando, function(data,status) {
		if ( status == "success" ) {
		    
		alert(comando);
        }
    });
}

// FUNCAO EXECUTADA NA CARGA DA APLICACAO
$(document).ready(function(){

// FUNÇÃO PARA INTERCEPTAR O CLICK DO BOTAO ID #btnCommando
$('#Comando').click (function() {setFormValues()});
$('#Users').click   (function() {getUsers()});
$('#Sala').click    (function() {getSalas()});
$('#Stealth').click (function() {setStealth()});

setBtnComando();
// mautec: teste atualizacao dos usuarios de uma sala
// mautec: teste atualizacao das salas disponiveis
var usuarios = ['USUARIO 1', 'USUARIO 2', 'USUARIO 3'];
var salas    = ['SALA A', 'SALA B', 'SALA C', 'SALA D'];
atualizaUsuarios(usuarios);
atualizaSalas   (salas);
});
