var express      = require('express');        // módulo express
var app          = express();                 // objeto express
var bodyParser   = require('body-parser');    // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
// mautec comentei pois alem de dar problema ficou redundante
//app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('public'));

var path = require('path'); // módulo usado para lidar com caminhos de arquivos

var proxies  = {}; // mapa de proxys
var proxy_id = 0;

// mautec: Sugestao para uma lista de objetos onde cada objeto contem o nome do canal 
// mautec:  e uma relacao dos ids que estao dentro do canal: como um indice reverso
var Canais  = [];
var user = [];


function proxy (id, servidor, nick, canal) {
    var cache = []; // cache de mensagens

    var irc_client = new irc.Client(
            servidor, 
            nick,
            {channels: [canal],});
	
	validaCanal(canal);	

    irc_client.addListener('message'+canal, function (from, message) {
        console.log(from + ' => '+ canal +': ' + message);
        cache.push( {"timestamp":Date.now(), 
                     "nick":from,
                     "msg":message} );
    });

    irc_client.addListener('error', function(message) {
        console.log('error: ', message);
    });

    irc_client.addListener('mode', function(message) {
        console.log('mode: ', message);
    });

  

    // matheus
    irc_client.addListener('channellist', function (channel_list) {
        for (var i = 0; i < 10; i++) {
            Canais.push(channel_list[i]);
        }
        terminou = true;
    });
    
    irc_client.addListener('join', function (channel, nick, message) {
        
        console.log(nick + ' entrou no canal: ' + channel);
    });
    irc_client.addListener('nick', function (oldnick, newnick, channels, message) {
        console.log('NICK: ' + oldnick + ' mudou para ' + newnick);
    });
    irc_client.addListener('names', function (channel, nicks) {
        console.log("Começou a lista");
        for (var i = 0; i < Object.keys(nicks).length; i++) {
            user.push(Object.keys(nicks)[i]);
        }

    });
    
      proxies[id] = { "cache":cache, "irc_client":irc_client  };
    return proxies[id];
}

app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
    proxy_id++;
    var p = proxy(  proxy_id,
            req.cookies.servidor,
            req.cookies.nick, 
            req.cookies.canal);
    res.cookie('id', proxy_id);
    res.sendFile(path.join(__dirname, '/index.html'));

    res.send("Bem vindo ao canal " + req.cookies.canal + ", " + req.cookies.nick + "!");
  }
  else {
        //res.sendFile(path.join(__dirname, '/login.html'));
        res.sendFile(path.join(__dirname, '/index.html'));
  }
});

app.get('/obter_mensagem/:timestamp', function (req, res) {
  var id = req.cookies.id;
  res.append('Content-type', 'application/json');
  res.send(proxies[id].cache);
});

app.post('/gravar_mensagem', function (req, res) {
  proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  irc_client.say(irc_client.opt.channels[0], req.body.msg );
  res.end();
});


app.get('/mode/:usuario/:args', function (req, res){
  var usuario = req.params.usuario;
  var args = req.params.args;
  var retorno = '{"usuario":'+usuario+','+'"args":"'+args+'}';
  res.send(retorno);
});
app.get('/mode/', function (req, res){
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", req.cookies.nick);
  res.send(retorno);
});


app.get('/names', function (req, res) { 
   var channel = req.cookies.canal;
   var nicks = [];   
  
   for (var proxy in proxies){
	   if (proxies[proxy].irc_client.indexOf(channel) != -1) {
		nicks.push(proxies[proxy].irc_client.nick);	
	   }
   }
   res.send(JSON.stringify(nicks));
});

app.get('/names/:channel', function (req, res) { 
   var channel = req.params.channel;
   var nicks = [];   
  
   for (var proxy in proxies){
	   if (proxies[proxy].irc_client.indexOf(channel) != -1) {
		nicks.push(proxies[proxy].irc_client.nick);	
	   }
   }
   res.send(JSON.stringify(nicks));
});

app.get('channellist_item', function (req, res) { 
   var canais = [];   
   for (var canal in Canais){
		canais.push(canal);	
   }
   res.send(JSON.stringify(canais));
});

app.get('channellist', function (req, res) { 
   var irc_client = proxies[req.cookies.id].irc_client;   
   res.send(JSON.stringify(irc_client.channellist));
});

app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

//matheus

app.get('/opcao/lista_canais', function (req, res) {
    var irc_client = proxies[req.cookies.id].irc_client;
    irc_client.list(req.cookies.servidor);
    terminou = false;
    res.send(comecou);
});

app.get('/opcao/mudar_nick/:args', function (req, res) {
    var arg = req.params.args;
    var irc_client = proxies[req.cookies.id].irc_client;
    irc_client.send("nick", arg);
    irc_client.emit("nick", req.cookies.nick, arg, req.cookies.canal);
    res.send(arg);
});

app.get('/opcao/entrar_canal/:args', function (req, res) {
    var arg = req.params.args;
    var irc_client = proxies[req.cookies.id].irc_client;
    arg = "#" + arg;
    irc_client.join(arg);
    irc_client.send("join", arg);
    irc_client.emit("join", arg, req.cookies.nick);
    var canal_atual = irc_client.opt.channels[0];
    irc_client.opt.channels[0] = arg;
    irc_client.opt.channels.push(canal_atual);
    res.send(arg);
});

app.get('/obter_list_usuario', function (req, res) {
    var irc_client = proxies[req.cookies.id].irc_client;
    var canal = req.cookies.canal;
    irc_client.send('names', canal);
    res.send(lista_usuarios);
});

app.get('/obter_lista', function (req, res) {
    if (!terminou) {
        res.send(terminou)
    } else {
        res.send(lista_canais);
    }

});

app.post('/topic/:arg', function (req, res) {
   var novo_topico = req.body.topico;
   var canal = Canais[req.params.args];
  if(canal != null) {   
    canal.topic = novo_topico;
  } else {
    res.send("Canal inválido");
  }
});

app.get('/topic/:arg', function (req, res) {
    var canal = Canais[req.params.args];
    if(canal != null) {   
      res.send(canal.nome + " <" + canal.topic + ">");
    } else {
      res.send("Canal inválido");
    }
});

app.listen(3000, function () {              
  console.log('Example app listening on port 3000!');   
});

function validaCanal(args) {
	//Adiciona na lista de canais caso não exista
	var canal = Canais[args];
	if(canal == null) {		
		Canais[canal] = { 'name' : canal, 'users' : 1, 'topic' : canal};
	} else {
		canal.users++;
	}
}

app.on('registered', function(req, res) {
    res.send("Olá " + req.cookies.nick + ", seja bem vindo!");    
});   

